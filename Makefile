# Executables
PHP      = php
COMPOSER = composer
SYMFONY  = php bin/console

# Misc
.DEFAULT_GOAL = help
.PHONY        = help build up start down logs sh composer vendor sf cc

# This is a minimal set of ANSI/VT100 color codes
_END=$'\x1b[0m'
_BOLD=$'\x1b[1m'
_UNDER=$'\x1b[4m'
_REV=$'\x1b[7m'

# Colors
GREEN = /bin/echo -e "\x1b[32m\#\# $1\x1b[0m"
RED = /bin/echo -e "\x1b[31m\#\# $1\x1b[0m"

## ——————  UTech Makerfile ——————
help: ## Outputs this help screen
	grep -E '(^[a-zA-Z0-9_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}{printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'

## —————— App ——————
init: ## Init the project
	$(MAKE) composer-install
	symfony server:start

cc: ## Clear cache
	$(SYMFONY) cache:clear

debug-router: ## Debug router
	$(SYMFONY) debug:router

entity: ## Make entity
	$(SYMFONY) make:entity

## —————— Composer 🐳 ——————
composer-install: ## Install packages
	$(COMPOSER) install --optimize-autoloader

composer-update: ## Update packages
	$(COMPOSER) update

composer-require: ## Require package
	$(COMPOSER) require

## —————— Tests & CI 🐳 ——————
phpunit-html-coverage: ## Run phpunit with html coverage
	$(PHP_CONT) vendor/bin/phpunit --coverage-html=coverage

phpcs: ## Run phpcs
	$(PHP_CONT) vendor/bin/phpcs -v --standard=PSR12 --ignore=./src/Kernel.php,./src/Migrations ./src

phpstan: ## Run phpstan
	$(PHP_CONT) vendor/bin/phpstan analyse -c phpstan.neon --memory-limit 1024M --error-format gitlab
