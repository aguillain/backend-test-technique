<?php

namespace App\Entity;

class Promotion
{
    public function __construct(
        private readonly PromotionCategory $category,
        private readonly float $rate
    ) {
    }

    /**
     * @return PromotionCategory
     */
    public function getCategory(): PromotionCategory
    {
        return $this->category;
    }

    /**
     * @return float
     */
    public function getRate(): float
    {
        return $this->rate;
    }

    public static function create(
        PromotionCategory $category,
        float $rate,
    ): self {
        return new self(
            category: $category,
            rate: $rate
        );
    }
}
