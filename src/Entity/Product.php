<?php

namespace App\Entity;

use Symfony\Component\Uid\Uuid;

class Product
{
    public function __construct(
        private string $name,
        private string $description,
        private int $quantity,
        private float $price,
        private ?Uuid $id = null
    ) {
        if (!$this->id) {
            $this->id = Uuid::v4();
        }
    }

    public function getId(): ?Uuid
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): static
    {
        $this->description = $description;

        return $this;
    }

    public function getQuantity(): int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): static
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getPrice(): float
    {
        return $this->price;
    }

    public function setPrice(float $price): static
    {
        $this->price = $price;

        return $this;
    }

    public static function create(
        string $name,
        string $description,
        int $quantity,
        float $price,
        ?Uuid $id = null
    ): self {
        return new self(
            name: $name,
            description: $description,
            quantity: $quantity,
            price: $price,
            id: $id
        );
    }
}
