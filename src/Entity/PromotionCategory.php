<?php

namespace App\Entity;

enum PromotionCategory: string
{
    case AMOUNT = 'A';
    case PERCENTAGE = 'P';
}
