<?php

namespace App\Controller\Product\CalculatePromotionalPrice;

use App\Entity\PromotionCategory;
use Symfony\Component\Validator\Constraints as Assert;

class PromotionQuery
{
    public function __construct(
        #[Assert\Choice(choices: [PromotionCategory::AMOUNT->value, PromotionCategory::PERCENTAGE->value])]
        private readonly string $category,
        #[Assert\PositiveOrZero]
        private readonly float $rate
    ) {
    }

    /**
     * @return string
     */
    public function getCategory(): string
    {
        return $this->category;
    }

    /**
     * @return float
     */
    public function getRate(): float
    {
        return $this->rate;
    }
}
