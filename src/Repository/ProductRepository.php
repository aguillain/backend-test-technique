<?php

namespace App\Repository;

use App\Entity\Product;
use App\Repository\Interface\ProductRepositoryInterface;
use Symfony\Component\Uid\Uuid;

class ProductRepository implements ProductRepositoryInterface
{
    /** @var Product[] $products */
    private array $products;

    public function __construct()
    {
        $this->products = [
            Product::create(
                name: 'Switch',
                description: 'Console nintendo',
                quantity: 5,
                price: 249.99,
                id: Uuid::fromString('4e24479c-5d14-4ad0-b0e0-7d12cdc022d8')
            ),
            Product::create(
                name: 'Playstation 5',
                description: 'Console sony',
                quantity: 0,
                price: 499.99,
                id: Uuid::fromString('998c7d3d-3b72-4fcf-a55f-684b06f06b31')
            ),
            Product::create(
                name: 'XBox One',
                description: 'Console microsoft',
                quantity: 0,
                price: 499.99,
                id: Uuid::fromString('fe6a002f-62a5-4075-934c-0f8092ba0a5f')
            ),
            Product::create(
                name: 'PC Gamer',
                description: 'PC Gamer',
                quantity: 5,
                price: 1499.99,
                id: Uuid::fromString('b4faa947-bf66-4a77-9f77-496303a38f78')
            ),
        ];
    }

    public function find(string $id): ?Product
    {
        return array_filter(
            $this->products,
            fn (Product $product) => $product->getId()?->toRfc4122() === $id
        )[0] ?? null;
    }

    /**
     * @return Product[]
     */
    public function findAll(): array
    {
        return $this->products;
    }

    public function save(Product $product): Product
    {
        return $product;
    }
}
